# Hur man skapar en "USB Rubber Ducky" för Samsung Galaxy S6 (SM-G920F)

## Innehållsförteckning
- [1. Bakgrund](#1-bakgrund)
  - [1.1 Vad är "USB Rubber Ducky"?](#11-vad-%C3%A4r-usb-rubber-ducky)
  - [1.2 Vad är "BadUSB"?](#12-vad-%C3%A4r-badusb)
  - [1.3 Hur implementerar man detta på en mobil](#13-hur-implementerar-man-detta-p%C3%A5-en-mobil)
- [2. Guide](#2-guide)
  - [2.1 Förhandskrav](#21-f%C3%B6rhandskrav)
	- [2.2 Förbered din utvecklingsmiljö](#22-f%C3%B6rbered-din-utvecklingsmilj%C3%B6)
	- [2.3 Patcha kerneln](#23-patcha-kerneln)
	  - [2.3.1 Automatisk](#231-automatiskt)
	  - [2.3.2 Manuellt](#232-manuellt-inte-n%C3%B6dv%C3%A4ndigt-ifall-du-anv%C3%A4nde-dig-av-den-automatiska-metoden)
	- [2.4 Konfigurera kerneln](#24-konfigurera-kerneln)
	- [2.5 Kompilera kerneln och bygg boot.img](#25-kompilera-kerneln-och-bygg-bootimg)
	- [2.6 Flasha boot.img](#26-flasha-bootimg)
	- [2.7 Testa](#27-testa)
	- [2.8 Lägg till Rubber Ducky-tolk](#28-l%C3%A4gg-till-rubber-ducky-tolk)
	- [2.9 Installera Bash](#29-installera-bash)
	- [2.10 Färdigställ](#210-f%C3%A4rdigst%C3%A4ll)
- [Källor](#k%C3%A4llor)
- [Programkodkällor](#programkodk%C3%A4llor)

## 1. Bakgrund

### 1.1 Vad är "USB Rubber Ducky"?
"USB Rubber Ducky" är en kommersiell produkt utvecklad av företaget "Hak5".
Hak5 är ett företag som specialiserar sig inom att utveckla produkter som används för att penetrationstesta datorer och andra digitala enheter.
De gör sig även tillgängliga genom sin YouTube-kanal "Hak5" ([https://www.youtube.com/user/Hak5Darren](https://www.youtube.com/user/Hak5Darren)) där de visar hur man bland annat använder sina produkter, och en massa andra guider.

Själva USB Rubber Ducky utnyttjar USB-protokollet, och hur datorer blint litar på USB-enheter. USB Rubber Ducky visar sig själv som ett tangentbord för datorn, men i själva verket är den en liten USB-sticka, som är programmerad till att utföra en rad knapptryckningar, vilket kan leda till att förövaren får full kontroll över offrets dator. Detta oavsiktliga uppförande har blivit kallad "BadUSB".

### 1.2 Vad är "BadUSB"?
Buggen BadUSB hittades utav en grupp tyska datasäkerhetsforskare, år 2014.
Den hittades genom att de programmerade om en microkontroller på ett USB-minne till att emulera ett tangentbord och utföra ett par tangentryckningar.
När man sedan pluggade in USB-minnet till en dator, så skulle USB-minnet agera som ett tangentbord, och utföra tangenttryckningarna som var programmerade, och eftersom att de flesta datorerna som finns idag kan kontrolleras med bara ett tangentbord, så är detta perfekt för att få full kontroll över en dator.

### 1.3 Hur implementerar man detta på en mobil?
Företaget "Offensive Security", skaparna av det mycket populära penetrationstest-verktyget "Kali Linux", har skapat ett verktyg till Android-baserade telefoner kallat "Kali NetHunter" eller bara "NetHunter".

Med NetHunter får man en rad olika intressanta verktyg som kan underlätta när man vill penetrationstesta ett system, och det mest intressanta i detta fallet kallas "DuckHunter HID".
DuckHunter HID tar syntaxen som används för USB Rubber Ducky, och gör om den tangenttryckningar, precis som USB Rubber Ducky, fast tangenttryckningarna är inte hårdkodade till programvaran i microkontrollern.
Genom DuckHunter HID får man även möjligheten att spara och ladda flera olika skript, som gör det enkelt att köra olika skript vid olika scenarion.

Problemet är bara att mobiltelefontillverkare skeppar inte sina telefoner med den programvaran och drivrutiner som behövs för att kunna utnyttja denna möjligheten i USB-protokollet.
NetHunter tar ansvar för att en del enheter har detta inbyggt i verktyget, men enheten Samsung Galaxy S6 lämnar de tyvärr inget stöd för.

Så därför bestämde jag mig för att implementera Rubber Ducky själv, fast utan NetHunter.

## 2. Guide

### 2.1 Förhandskrav
* En rootad Samsung Galaxy S6, med BusyBox och [TWRP](https://twrp.me/) eller [CWM](http://www.clockworkmod.com/) installerat
* En dator eller virtuell maskin med Linux-baserat distro ([Debian](https://www.debian.org/), [Kali](https://www.kali.org/), [Mint](https://linuxmint.com/), eller [Ubuntu](https://www.ubuntu.com/) föredraget)
* Git

### 2.2 Förbered din utvecklingsmiljö

Det första man behöver göra är att hämta alla resurser och verktyg för att kunna bygga din egen programvara/kernel.
I denna guiden kommer arter97's kernel för Samsung Galaxy S6 att användas.
arter97's kernel är lite mer prestandaoptimerad och har lite fler funktioner, samtidigt som den är batterisnålare än Samsungs ursprungliga kernel.
Källkoden till kerneln hämtas genom att köra följande kommando i terminalen:
```bash
git clone https://bitbucket.org/arter97/android_kernel_samsung_exynos7420
```

Det behövs även en toolchain för att kunna kompilera kerneln till telefonen.
I denna guide kommer Linaro att användas som toolchain.
Linaros toolchain laddas ned från: [https://releases.linaro.org/components/toolchain/binaries/6.2-2016.11/aarch64-linux-gnu/](https://releases.linaro.org/components/toolchain/binaries/6.2-2016.11/aarch64-linux-gnu/)

När all källkod och toolchainen har hämtats måste man konfigurera kompilatorn att använda rätt toolchain när man kompilerar sin kernel.
Redigera Makefile i `android_kernel_samsung_exynos7420` så att `CROSS_COMPILE` på rad 201 pekar mot din toolchains binärer, och redigera rad 396 så att `KBUILD_CFLAGS` inte innehåller `-mno-android`.

Nu är kerneln redo för att patchas.

### 2.3 Patcha kerneln

#### 2.3.1 Automatiskt
För över `samsung-galaxy-s6-usb-hid.patch` från denna repositoryn till din kernel-mapp, och patcha sedan kerneln:
```bash
patch -p1 < samsung-galaxy-s6-usb-hid.patch
```
Gå sedan till steg tre.

#### 2.3.2 Manuellt (Inte nödvändigt ifall du använde dig av den automatiska metoden)

Ladda ner [https://raw.githubusercontent.com/pelya/android-keyboard-gadget/master/kernel-3.10-nexus6.patch](https://raw.githubusercontent.com/pelya/android-keyboard-gadget/master/kernel-3.10-nexus6.patch), och patcha sedan kerneln:
```bash
patch -p1 < kernel-3.10-nexus6.patch
```
Tyvärr är inte hela patchen kompatibel, så öppna filen `drivers/usb/gadget/android.c.rej` som `patch` lämnar efter sig.
Innuti filen kan man kan se att en del utav patchen inte kunde patchas automatisk, därför måste det göras manuellt.

`patch` syntaxen fungerar på det sättet att den noterar en ny rad med ett `+` och en borttagen rad med `-`. Före och efter `+` och `-`-notationerna kan man se ett par rader kod som gör det enklare att urskilja vart de nya raderna kod ska läggas.

På rad 44-46 i `drivers/usb/gadget/android.c` lägg till:
```c
#include "f_hid.h"
#include "f_hid_android_keyboard.c"
#include "f_hid_android_mouse.c"
```

På rad 1667 i `drivers/usb/gadget/android.c` lägg till:
```c
&hid_function,
```

På rad 1894 i `drivers/usb/gadget/android.c` lägg till:
```c
android_enable_function(dev, "hid");
```

Och till sist på rad 4-5 i `drivers/usb/gadget/Makefile` lägg till;
```Makefile
KBUILD_CFLAGS += -w
KBUILD_CFLAGS += -Wno-error=unused-but-set-variable
```

Sedan är patchen färdig.

Nu kan man även (om man vill) generera en egen patch med:
```bash
rm -rf drivers/usb/gadget/android.c.*
git add drivers/
git commit -m "Patched USB-HID"
git show > galaxy-s6-usb-hid.patch
```

### 2.4 Konfigurera kerneln

Börja med att kopiera arter97's `defconfig` till `.config` vilket är standardkonfigurationen som används i arter97's kernel:
```bash
cp defconfig .config
```

Kör sedan igång `menuconfig` (se till att du har `ncurses`-biblioteket installerat):
```bash
make menuconfig
```

Gå in på menyn `General setup` och konfigurera `System V IPC` till att vara på genom att trycka på `Mellanslag`.

Gå sedan ut från `General setup` och konfigurera `Enable loadable module support` till på-läget och gå sedan in på menyn.

Innuti menyn `Enable loadable module support` så konfigurerar man `Forced module loading`, `Module unloading`, och `Forced module unloading` till att vara på.

Spara sedan konfigurationen och gå ut ur `menuconfig`.

### 2.5 Kompilera kerneln och bygg `boot.img`

Kör igång `make` för att kompilera kerneln (detta tar tid):
```bash
make
```

Kör sedan följande för att bygga `boot.img` (`skip` är för att hoppa över kernelkompileringen, som redan är utförd):
```bash
./build_kernel.sh skip
```

Lägg sedan in `boot.img` på din telefon så är den redo för att flashas

### 2.6 Flasha `boot.img`

Starta upp din telefon i `Recovery mode` och flasha din kernel. I moderna "återställningshanterare" (som TWRP) så har man möjligheten att manuellt flasha råa diskavbilder (img-filer). Denna guiden antar att du redan kan navigera din återställningshanterare för din telefon.

Efter att du har flashat avbilden startar du om telefonen.

### 2.7 Testa

Ladda ner [https://github.com/pelya/android-keyboard-gadget/raw/master/USB-Keyboard.apk](https://github.com/pelya/android-keyboard-gadget/raw/master/USB-Keyboard.apk) och installera den på din telefon.

Koppla sedan in din telefon via USB, och vänta eventuellt på att datorn installerar nya drivrutiner. Se till att din dator har tillgång till din telefons SD-kort, d.v.s. att telefonen är inställd till MTP-läget.

Sedan kan du starta upp den nyss installerade appen och testa dess funktionalitet.

Förhoppningsvist har du nu en fungerande mus/tangenbords-anslutning mellan din dator och telefon.

(OBS! Det är viktigt att du avslutar appen innan du kopplar ur den från datorn, ifall detta inte görs, kan det hända att du får problem vid nästa anslutning till datorn. Vanligtvist brukar en omstart av telefonen hjälpa, ifall du skulle glömma.)

### 2.8 Lägg till Rubber Ducky-tolk

I denna guiden kommer vi att använda oss utav den redan färdiga Rubber Ducky-tolken "DroidDucky". Den kan laddas ner från [https://github.com/anbud/DroidDucky](https://github.com/anbud/DroidDucky). Klona repositoryn med:
```bash
git clone https://github.com/anbud/DroidDucky
```
och lägg sedan filerna i en ny mapp på din telefon.

Ifall du inte redan har det, så är det hög tid att installera en terminal emulator på din telefon nu. En enkel sökning efter "terminal emulator" på Play Store kommer att ge dig flertal olika terminaler som fungerar bra. Själv rekommenderar jag terminalen "Terminal Emulator for Android" av Jack Palevich. Öppna sedan terminalen och navigera dig till den mappen som du lade Rubber Ducky-skripten. Kör sedan följande för att gå in i root-läge:
```bash
su
```

Nu måste Ducky-skripten flyttas från SD-kortet till `data`-partitionen eftersom att Android inte tillåter filer att bli exerkverade från SD-kortet. Jag väljer att lägga skripten i `/data/local`-mappen. Ifall mappen inte finns kan du skapa den med:
```bash
mkdir -p /data/local
```
Flytta Ducky-tolken:
```bash
mv hid-gadget-test droidducky.sh /data/local
```
Kör sedan:
```bash
cd /data/local
chmod +x hid-gadget-test droidducky.sh
```

### 2.9 Installera Bash

Skriptet som används för att tolka Rubber Ducky-skripten är programmerat i Bash, så därför behövs en Bash-tolk installerat på din mobil. Börja med att ladda ner filen [https://github.com/nubecoder/android_bash/raw/master/android_bash-4.1.11(2)](https://github.com/nubecoder/android_bash/raw/master/android_bash-4.1.11%282%29) och lägg sedan över filen till ditt SD-kort.

Sedan på mobilen så öppnar du upp terminalen igen, och sen kör följande för att köra kommandon som `root`:
```bash
su
```
Montera om `system`-partitionen till läs/skriv-läge:
```bash
mount -o remount,rw /system
```
Flytta Bash och gör den exekverbar:
```bash
mv bash /system/xbin/bash
chmod +x /system/xbin/bash
```
Montera om `system`-partitionen till skrivskydd-läge igen:
```bash
mount -o remount,ro /system
```

### 2.10 Färdigställ

Nu är din telefon redo!

Ladda ner en payload från https://github.com/hak5darren/USB-Rubber-Ducky/wiki/Payloads, eller skapa din egen genom att följa syntaxen https://github.com/hak5darren/USB-Rubber-Ducky/wiki/Duckyscript. Lägg sedan din payload i en textfil på SD-kortet. Därefter kan du köra följande för att utföra payloaden:
```bash
bash droidducky.sh [payload.txt]
```
Till exempel:
```bash
bash droidducky.sh /sdcard/ducky/helloworld.txt
```
Se till att ha telefonen inkopplad till datorn i MTP-läget, du är i `/data/local`-katalogen, och även att köra terminalprompten som `root`.
Kommandot `su` kör terminalprompten som root.

Ha roligt, lär dig saker, och använd verktyget ansvarsfullt!

## Källor

https://hakshop.com/products/usb-rubber-ducky-deluxe

https://www.hak5.org/about

https://srlabs.de/bites/usb-peripherals-turn/

https://opensource.srlabs.de/projects/badusb


https://www.kali.org/about-us/

https://www.kali.org/kali-linux-nethunter/


## Programkodkällor

https://bitbucket.org/arter97/android_kernel_samsung_exynos7420

https://github.com/pelya/android-keyboard-gadget

https://github.com/anbud/DroidDucky

https://github.com/nubecoder/android_bash
